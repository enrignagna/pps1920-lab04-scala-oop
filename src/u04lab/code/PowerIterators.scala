package u04lab.code

import Optionals._
import Lists._

trait PowerIterator[A] {
  def next(): Option[A]
  def allSoFar(): List[A]
  def reversed(): PowerIterator[A]
}

trait PowerIteratorsFactory {

  def incremental(start: Int, successive: Int => Int): PowerIterator[Int]
  def fromList[A](list: List[A]): PowerIterator[A]
  def randomBooleans(size: Int): PowerIterator[Boolean]
}

class PowerIteratorsFactoryImpl extends PowerIteratorsFactory {
  import Stream._
  var stream = new Stream() {
    override protected def tailDefined: Boolean = ???
  }
  override def incremental(start: Int, successive: Int => Int): PowerIterator[Int] = {
    Stream.iterate(start)(successive)
  }
  override def fromList[A](list: List[A]): PowerIterator[A] = ???

  override def randomBooleans(size: Int): PowerIterator[Boolean] = ???
}
